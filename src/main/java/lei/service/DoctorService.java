package lei.service;

import lei.entity.Doctor;
import lei.query.LimitQuery;
import lei.utils.ResponseData;

import java.util.List;

/**
 * (Doctor)表服务接口
 *
 * @author makejava
 * @since 2021-08-19 09:59:59
 */
public interface DoctorService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Doctor queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Doctor> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param doctor 实例对象
     * @return 实例对象
     */
    Doctor insert(Doctor doctor);

    /**
     * 修改数据
     *
     * @param doctor 实例对象
     * @return 实例对象
     */
    Doctor update(Doctor doctor);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

    /**
     * 分页获取医生信息
     * @param limitQuery
     * @return
     */
    ResponseData queryDocInfo(LimitQuery limitQuery);

    /**
     *
     * @param deptname
     * @return
     */
    ResponseData queryDocByDept(String deptname,LimitQuery limitQuery);

    /**
     *
     * @param keyword
     * @return 查询数据
     */
    ResponseData queryDocByKeyword(String keyword,String deptname);


    /**
     *
     * @param id
     * @return
     */
    ResponseData queryInfoByDocId(Long id);

}
