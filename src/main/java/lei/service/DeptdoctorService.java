package lei.service;

import lei.entity.Deptdoctor;

import java.util.List;

/**
 * (Deptdoctor)表服务接口
 *
 * @author makejava
 * @since 2021-08-19 09:59:58
 */
public interface DeptdoctorService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Deptdoctor queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Deptdoctor> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param deptdoctor 实例对象
     * @return 实例对象
     */
    Deptdoctor insert(Deptdoctor deptdoctor);

    /**
     * 修改数据
     *
     * @param deptdoctor 实例对象
     * @return 实例对象
     */
    Deptdoctor update(Deptdoctor deptdoctor);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
