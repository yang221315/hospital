package lei.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lei.entity.User;
import lei.dao.UserDao;
import lei.query.HttpClientUtil;
import lei.query.Secretity;
import lei.service.UserService;
import lei.utils.ResponseCode;
import lei.utils.ResponseData;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (User)表服务实现类
 *
 * @author makejava
 * @since 2021-08-19 10:00:02
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public User queryById(Long id) {
        return this.userDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<User> queryAllByLimit(int offset, int limit) {
        return this.userDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public ResponseData insert(User user) {
        String phone = user.getPhone();
        String password = user.getPassword();
        try{
            //1.对手机号和密码进行 非空 校验
            if(Secretity.IsNull(phone) || Secretity.IsNull(password)){
                return new ResponseData(ResponseCode.ERROR1);
            }
            //2.校验手机号是否已被注册
            User queryUser = userDao.queryUserByPhone(phone);
            if(queryUser!=null){    //说明手机号已被注册
                return new ResponseData(ResponseCode.ERROR2);
            }
            //3.对密码再加密
            Md5Hash md5Hash = new Md5Hash(password,"xiucaozuo",10);
            //4.保存密码
            user.setPassword(md5Hash.toString());
            this.userDao.insert(user);
            return new ResponseData(ResponseCode.SUCCESS);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User update(User user) {
        this.userDao.update(user);
        return this.queryById(user.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.userDao.deleteById(id) > 0;
    }

    /**
     * 通过code password phone
     * @param code
     * @param password
     * @param phone
     * @return
     */
    @Override
    public ResponseData userLogin(String code, String password, String phone) {
        //1.非空校验
        //校验code
        if(Secretity.IsNull(code)){
            return new ResponseData(ResponseCode.ERROR3);
        }
        //校验password和phone
        if(Secretity.IsNull(password) || Secretity.IsNull(phone)){
            return new ResponseData(ResponseCode.ERROR1);
        }

        try{
            //2.做密码加密  加密规则 盐值 加密次数要一致
            Md5Hash md5Hash = new Md5Hash(password,"xiucaozuo",10);
            password =md5Hash.toString();   //获取加密后的密码

            //3.查看是否与数据库的数据匹配
            Map<String,Object> map = new HashMap<>();
            map.put("phone",phone);
            map.put("password",password);
            User user = userDao.queryUserByMap(map);
            if(user==null){//未查到数据
                return new  ResponseData(ResponseCode.FAIL);
            }

            //4.调用微信的登录接口，模拟http请求
            String httpUrl = "https://api.weixin.qq.com/sns/jscode2session?appid="+Secretity.APPID+
                    "&secret="+Secretity.AppSecret+"&js_code="+code+"&grant_type=authorization_code";
            String result = HttpClientUtil.doGet(httpUrl);
            System.out.println("result = "+result);

            //5.把字符串转化成json对象
            JSONObject jsonObject = (JSONObject) JSON.parse(result);
            String session_key = (String)jsonObject.get("session_key");
            String openid = (String)jsonObject.get("openid");
            System.out.println("session_key = "+session_key+",openid = "+openid);

            //6.自定义登录状态  需要关联session_key和openid
            Md5Hash md5 = new Md5Hash(openid,session_key,100);
            String token = md5.toString();

            //7.做更新操作  根据手机号更新user表  session_key openid token
            user.setToken(token);
            user.setSessionkey(session_key);
            user.setOpenid(openid);
            userDao.update(user);

            return new ResponseData(token,ResponseCode.SUCCESS);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }

    @Override
    public String queryOpenidByToken(String token) {
        return userDao.queryOpenidByToken(token);
    }
}
