package lei.service.impl;

import lei.entity.Doctor;
import lei.dao.DoctorDao;
import lei.query.LimitQuery;
import lei.service.DoctorService;
import lei.utils.ResponseCode;
import lei.utils.ResponseData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Doctor)表服务实现类
 *
 * @author makejava
 * @since 2021-08-19 09:59:59
 */
@Service("doctorService")
public class DoctorServiceImpl implements DoctorService {
    @Resource
    private DoctorDao doctorDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Doctor queryById(Long id) {
        return this.doctorDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Doctor> queryAllByLimit(int offset, int limit) {
        return null;
    }

    /**
     * 新增数据
     *
     * @param doctor 实例对象
     * @return 实例对象
     */
    @Override
    public Doctor insert(Doctor doctor) {
        this.doctorDao.insert(doctor);
        return doctor;
    }

    /**
     * 修改数据
     *
     * @param doctor 实例对象
     * @return 实例对象
     */
    @Override
    public Doctor update(Doctor doctor) {
        this.doctorDao.update(doctor);
        return this.queryById(doctor.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.doctorDao.deleteById(id) > 0;
    }

    @Override
    public ResponseData queryDocInfo(LimitQuery limitQuery) {
        try{
            Long page = limitQuery.getPage();
            Long limit = limitQuery.getLimit();
            Long start = (page - 1) * limit;
            List<Doctor> doctors = doctorDao.queryAllByLimit(start, limit);
            //获取总条数
            Long count = doctorDao.queryCount();
            return new ResponseData(doctors, ResponseCode.SUCCESS,count);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }

    @Override
    public ResponseData queryDocByDept(String deptname,LimitQuery limitQuery)
    {
        try
        {
            Long page = limitQuery.getPage();
            Long limit = limitQuery.getLimit();
            Long start = (page - 1) * limit;
            Long count = doctorDao.queryDocByType(deptname);
            List<Doctor> doctorList = doctorDao.queryDocByDept(deptname,start, limit);
            return new ResponseData(doctorList,ResponseCode.SUCCESS,count);
        }
        catch (Exception e)
        {
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }

    @Override
    public ResponseData queryDocByKeyword(String keyword,String deptname) {
        try{
            List<Doctor> doctorList = doctorDao.queryDocByKeyword("%"+keyword+"%",deptname);
            return new ResponseData(doctorList,ResponseCode.SUCCESS);
        }
        catch (Exception e)
        {
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }

    @Override
    public ResponseData queryInfoByDocId(Long id) {
        try
        {
            List<Doctor> doctorList = doctorDao.queryInfoByDocId(id);
            return new ResponseData(doctorList,ResponseCode.SUCCESS);
        }
        catch (Exception e)
        {
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }
}
