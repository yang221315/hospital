package lei.service.impl;

import lei.entity.Doctor;
import lei.entity.Order;
import lei.dao.OrderDao;
import lei.service.OrderService;
import lei.utils.ResponseCode;
import lei.utils.ResponseData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * (Order)表服务实现类
 *
 * @author makejava
 * @since 2021-08-19 10:00:01
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderDao orderDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Order queryById(Long id) {
        return this.orderDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Order> queryAllByLimit(int offset, int limit) {
        return this.orderDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    @Override
    public ResponseData insert(Order order) {
        try{
            //1.设置状态为0
            order.setOrderstate("0");
            //2.可以随机分配一个技师   随机数
            this.orderDao.insert(order);
            return new ResponseData(ResponseCode.SUCCESS);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    @Override
    public Order update(Order order) {
        this.orderDao.update(order);
        return this.queryById(order.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.orderDao.deleteById(id) > 0;
    }

    @Override
    public ResponseData queryOpenidByOpenidAndOrderstate(String openid, String orderstate) {
        try{
            Order order = new Order();
            order.setOpenid(openid);
            order.setOrderstate(orderstate);

            List<Order> orders = orderDao.queryAll(order);
            List<Order> orderList = new ArrayList<>();

            for(int i=0;i<orders.size();i++)
            {
                System.out.println(orders.get(i).toString());
                Order order1 = new Order();
                if(orders.get(i).getDoctorid() == 0)
                {
                    order1 = orderDao.queryDeptById(orders.get(i).getDeptid());
                    order1.setId(orders.get(i).getId());
                    order1.setName(order1.getDepartment().getDeptname());
                    order1.setUrl(order1.getDepartment().getImageurl());
                    System.out.println("dept-->"+order1.toString());
                }

                if(orders.get(i).getDeptid() == 0)
                {
                    order1 = orderDao.queryDocById(orders.get(i).getDoctorid());
                    order1.setId(orders.get(i).getId());
                    order1.setName(order1.getDoctor().getDoctorname());
                    order1.setUrl(order1.getDoctor().getImage().getImageurl());
                    System.out.println("doc-->"+order1.toString());
                }

                if(orders.get(i).getDeptid() != 0  && orders.get(i).getDoctorid() != 0)
                {
                    order1 = orderDao.queryDocById(orders.get(i).getDoctorid());
                    order1.setId(orders.get(i).getId());
                    order1.setName(order1.getDoctor().getDoctorname());
                    order1.setUrl(order1.getDoctor().getImage().getImageurl());
                    System.out.println("doc-->"+order1.toString());
                }

                orderList.add(order1);
            }

            return new ResponseData(orderList,ResponseCode.SUCCESS);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }
}
