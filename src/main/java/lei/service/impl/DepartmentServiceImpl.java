package lei.service.impl;

import lei.entity.Department;
import lei.dao.DepartmentDao;
import lei.query.LimitQuery;
import lei.service.DepartmentService;
import lei.utils.ResponseCode;
import lei.utils.ResponseData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Department)表服务实现类
 *
 * @author makejava
 * @since 2021-08-19 09:59:56
 */
@Service("departmentService")
public class DepartmentServiceImpl implements DepartmentService {
    @Resource
    private DepartmentDao departmentDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Department queryById(Long id) {
        return this.departmentDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @return 对象列表
     */
    @Override
    public ResponseData queryAllByLimit(LimitQuery limitQuery) {
        try {
            Long page = limitQuery.getPage();
            Long limit = limitQuery.getLimit();

            Long start = (page-1)*limit;
            List<Department> departments = this.departmentDao.queryAllByLimit(start, limit);
            //获取数据库的总条数
            Long count = departmentDao.queryCount();
            return new ResponseData(departments, ResponseCode.SUCCESS,count);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }

    /**
     * 新增数据
     *
     * @param department 实例对象
     * @return 实例对象
     */
    @Override
    public Department insert(Department department) {
        this.departmentDao.insert(department);
        return department;
    }

    /**
     * 修改数据
     *
     * @param department 实例对象
     * @return 实例对象
     */
    @Override
    public Department update(Department department) {
        this.departmentDao.update(department);
        return this.queryById(department.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.departmentDao.deleteById(id) > 0;
    }

    /**
     * 通过主键id查询数据
     * @param id
     * @return
     */
    @Override
    public ResponseData queryDepInfoById(Long id) {
        try {
            Department department = departmentDao.queryById(id);
            return  new ResponseData(department,ResponseCode.SUCCESS);

        }catch (Exception e){
            System.out.println(e);
            return new ResponseData(ResponseCode.FAIL);
        }
    }
}
