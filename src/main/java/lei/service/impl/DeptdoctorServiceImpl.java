package lei.service.impl;

import lei.entity.Deptdoctor;
import lei.dao.DeptdoctorDao;
import lei.service.DeptdoctorService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Deptdoctor)表服务实现类
 *
 * @author makejava
 * @since 2021-08-19 09:59:58
 */
@Service("deptdoctorService")
public class DeptdoctorServiceImpl implements DeptdoctorService {
    @Resource
    private DeptdoctorDao deptdoctorDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Deptdoctor queryById(Long id) {
        return this.deptdoctorDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Deptdoctor> queryAllByLimit(int offset, int limit) {
        return this.deptdoctorDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param deptdoctor 实例对象
     * @return 实例对象
     */
    @Override
    public Deptdoctor insert(Deptdoctor deptdoctor) {
        this.deptdoctorDao.insert(deptdoctor);
        return deptdoctor;
    }

    /**
     * 修改数据
     *
     * @param deptdoctor 实例对象
     * @return 实例对象
     */
    @Override
    public Deptdoctor update(Deptdoctor deptdoctor) {
        this.deptdoctorDao.update(deptdoctor);
        return this.queryById(deptdoctor.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.deptdoctorDao.deleteById(id) > 0;
    }
}
