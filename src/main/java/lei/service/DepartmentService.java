package lei.service;

import lei.entity.Department;
import lei.query.LimitQuery;
import lei.utils.ResponseData;

import java.util.List;

/**
 * (Department)表服务接口
 *
 * @author makejava
 * @since 2021-08-19 09:59:56
 */
public interface DepartmentService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Department queryById(Long id);

    /**
     * 查询多条数据
     * @param limitQuery
     * @return 对象列表
     */
    ResponseData queryAllByLimit(LimitQuery limitQuery);

    /**
     * 新增数据
     *
     * @param department 实例对象
     * @return 实例对象
     */
    Department insert(Department department);

    /**
     * 修改数据
     *
     * @param department 实例对象
     * @return 实例对象
     */
    Department update(Department department);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

    /**
     * 通过主键id查询数据
     * @param id
     * @return
     */
    ResponseData queryDepInfoById(Long id);

}
