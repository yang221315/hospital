package lei.utils;

public enum ResponseCode
{
    SUCCESS("0","请求成功"),
    FAIL("444","请求失败,重试"),
    ERROR1("405","存在为空的数据"),
    ERROR3("407","登录异常"),
    ERROR2("406","数据异常");

    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    ResponseCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
