package lei.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lei.entity.User;
import lei.service.UserService;
import lei.utils.ResponseData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (User)表控制层
 *
 * @author makejava
 * @since 2021-08-19 10:00:02
 */
@Api(tags = "用户")
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ApiOperation(value = "用户查询",notes = "用户id查询")
    public User selectOne(Long id) {
        return this.userService.queryById(id);
    }

    @ApiOperation(value = "用户注册信息",notes = "用户注册信息")
    @PostMapping("regUser")
    public ResponseData regUser(User user){
        return userService.insert(user);
    }

    @ApiOperation(value = "用户登录信息",notes = "用户登录信息")
    @PostMapping("userLogin")
    public ResponseData userLogin(String code,String password,String phone)
    {
        return userService.userLogin(code,password,phone);
    }
}
