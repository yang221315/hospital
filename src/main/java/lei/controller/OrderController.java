package lei.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lei.entity.Order;
import lei.query.Secretity;
import lei.service.OrderService;
import lei.service.UserService;
import lei.utils.ResponseCode;
import lei.utils.ResponseData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Order)表控制层
 *
 * @author makejava
 * @since 2021-08-19 10:00:01
 */
@RestController
@RequestMapping("order")
@Api(tags = "订单")
public class OrderController {
    /**
     * 服务对象
     */
    @Resource
    private OrderService orderService;
    @Resource
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ApiOperation(value = "订单id查询",notes = "根据订单id号查询")
    public Order selectOne(Long id) {
        return this.orderService.queryById(id);
    }

    /**
     * 获取订单信息
     * @param order
     * @param token
     * @return
     */
    @PostMapping("addOrder")
    @ApiOperation(value = "保存订单信息",notes = "通过前台传入的数据，保存订单的详细信息")
    public ResponseData addOrder(Order order, String token){
        System.out.println("order = "+order);
        System.out.println("token = "+token);
        //1.根据token获取到openid
        String openid = userService.queryOpenidByToken(token);

        if(Secretity.IsNull(openid)){
            return new ResponseData(ResponseCode.FAIL);
        }
        System.out.println("获取到的openid = "+openid);
        order.setOpenid(openid);
        //调用保存
        return orderService.insert(order);
    }

    /**
     *
     * @param token
     * @param orderstate
     * @return
     */
    @GetMapping("queryOrderByType")
    @ApiOperation(value = "获取订单信息",notes = "根据订单状态获取订单的详细信息")
    public ResponseData queryOrderByType(String token,String orderstate){
        System.out.println("token = "+token);
        System.out.println("orderstate = "+orderstate);
        //1.根据token获取到openid
        String openid = userService.queryOpenidByToken(token);

        return orderService.queryOpenidByOpenidAndOrderstate(openid,orderstate);
    }

}
