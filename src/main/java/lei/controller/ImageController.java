package lei.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lei.entity.Image;
import lei.service.ImageService;
import lei.utils.ResponseData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Image)表控制层
 *
 * @author makejava
 * @since 2021-08-19 10:00:00
 */
@RestController
@RequestMapping("image")
@Api(tags = "医生科室图片")
public class ImageController {
    /**
     * 服务对象
     */
    @Resource
    private ImageService imageService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ApiOperation(value = "图片信息",notes = "根据图片id查询")
    public Image selectOne(Long id) {
        return this.imageService.queryById(id);
    }

    /**
     * 通过图片类型查询图片数据
     * @param imagetype
     * @return
     */
    @ApiOperation(value = "获取图片信息",notes = "根据图片类型获取图片信息")
    @GetMapping("queryImageByImagetype")
    public ResponseData queryImageByImagetype(String imagetype){

        return imageService.queryImageByImagetype(imagetype);
    }

}
