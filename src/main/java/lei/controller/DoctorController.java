package lei.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lei.entity.Doctor;
import lei.query.LimitQuery;
import lei.service.DoctorService;
import lei.utils.ResponseData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Doctor)表控制层
 *
 * @author makejava
 * @since 2021-08-19 09:59:59
 */
@RestController
@RequestMapping("doctor")
@Api(tags = "医生")
public class DoctorController {
    /**
     * 服务对象
     */
    @Resource
    private DoctorService doctorService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ApiOperation(value = "id查询",notes = "根据医生的id查询")
    public Doctor selectOne(Long id) {
        return this.doctorService.queryById(id);
    }

    @ApiOperation(value = "分页获取医生信息",notes = "分页获取医生信息")
    @GetMapping("queryDocInfo")
    public ResponseData queryDocInfo(LimitQuery limitQuery)
    {
        return doctorService.queryDocInfo(limitQuery);
    }

    @ApiOperation(value = "医生信息" ,notes = "医生信息详情")
    @GetMapping("queryDocByDept")
    public ResponseData queryDocByDept(String deptname,LimitQuery limitQuery)
    {
        return doctorService.queryDocByDept(deptname,limitQuery);
    }

    @GetMapping("queryDocByKeyword")
    @ApiOperation(value = "关键字查询",notes = "根据关键字进行模糊查询")
    public ResponseData queryDocByKeyword(String keyword,String deptname)
    {
        return doctorService.queryDocByKeyword(keyword,deptname);
    }

    @GetMapping("queryInfoByDocId")
    @ApiOperation(value = "医生信息获取",notes = "在预订页面通过医生id获取信息")
    public ResponseData queryInfoByDocId(Long id)
    {
        return doctorService.queryInfoByDocId(id);
    }
}
