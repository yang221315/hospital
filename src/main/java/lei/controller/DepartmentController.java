package lei.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lei.entity.Department;
import lei.query.LimitQuery;
import lei.service.DepartmentService;
import org.springframework.web.bind.annotation.*;
import lei.utils.ResponseData;

import javax.annotation.Resource;

/**
 * (Department)表控制层
 *
 * @author makejava
 * @since 2021-08-19 09:59:57
 */
@Api(tags = "科室")
@RestController
@RequestMapping("department")
public class DepartmentController {
    /**
     * 服务对象
     */
    @Resource
    private DepartmentService departmentService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ApiOperation(value = "id查询",notes = "根据id查询")
    public Department selectOne(Long id) {
        return this.departmentService.queryById(id);
    }

    /**
     * 通过页数和每页条数获取数据
     * @param limitQuery
     * @return
     */
    @ApiOperation(value = "分页获取产品信息",notes = "在首页产品栏分页获取产品信息")
    @GetMapping("queryDepInfo")
    public ResponseData queryDepInfo(LimitQuery limitQuery)
    {
        return departmentService.queryAllByLimit(limitQuery);
    }

    /**
     * 根据id获取科室信息
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id获取信息",notes = "根据id获取信息")
    @GetMapping("queryDepInfoById")
    public ResponseData queryDepInfoById(Long id)
    {
        return departmentService.queryDepInfoById(id);
    }

}
