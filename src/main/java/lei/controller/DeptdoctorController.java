package lei.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lei.entity.Deptdoctor;
import lei.service.DeptdoctorService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Deptdoctor)表控制层
 *
 * @author makejava
 * @since 2021-08-19 09:59:59
 */
@RestController
@RequestMapping("deptdoctor")
@Api(tags = "科室下属医生")
public class DeptdoctorController {
    /**
     * 服务对象
     */
    @Resource
    private DeptdoctorService deptdoctorService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ApiOperation(value = "科室和医生关联",notes = "科室与医生的关联表")
    public Deptdoctor selectOne(Long id) {
        return this.deptdoctorService.queryById(id);
    }

}
