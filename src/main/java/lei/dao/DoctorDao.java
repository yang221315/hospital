package lei.dao;

import lei.entity.Doctor;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Doctor)表数据库访问层
 *
 * @author makejava
 * @since 2021-08-19 09:59:59
 */
public interface DoctorDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Doctor queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Doctor> queryAllByLimit(@Param("offset") Long offset, @Param("limit") Long limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param doctor 实例对象
     * @return 对象列表
     */
    List<Doctor> queryAll(Doctor doctor);

    /**
     * 新增数据
     *
     * @param doctor 实例对象
     * @return 影响行数
     */
    int insert(Doctor doctor);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Doctor> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Doctor> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Doctor> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Doctor> entities);

    /**
     * 修改数据
     *
     * @param doctor 实例对象
     * @return 影响行数
     */
    int update(Doctor doctor);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    /**
     * 获取总条数
     * @return
     */
    Long queryCount();

    /**
     *
     * @param deptname
     * @return
     */
    List<Doctor> queryDocByDept(String deptname,Long offset,Long limit);

    /**
     *
     * @param deptname
     * @return
     */
    Long queryDocByType(String deptname);

    /**
     *
     * @param keyword
     * @param deptname
     * @return
     */
    List<Doctor> queryDocByKeyword(String keyword,String deptname);

    /**
     *
     * @param id
     * @return
     */
    List<Doctor> queryInfoByDocId(Long id);

}

