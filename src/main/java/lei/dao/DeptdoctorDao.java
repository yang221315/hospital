package lei.dao;

import lei.entity.Deptdoctor;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Deptdoctor)表数据库访问层
 *
 * @author makejava
 * @since 2021-08-19 09:59:58
 */
public interface DeptdoctorDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Deptdoctor queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Deptdoctor> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param deptdoctor 实例对象
     * @return 对象列表
     */
    List<Deptdoctor> queryAll(Deptdoctor deptdoctor);

    /**
     * 新增数据
     *
     * @param deptdoctor 实例对象
     * @return 影响行数
     */
    int insert(Deptdoctor deptdoctor);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Deptdoctor> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Deptdoctor> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Deptdoctor> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Deptdoctor> entities);

    /**
     * 修改数据
     *
     * @param deptdoctor 实例对象
     * @return 影响行数
     */
    int update(Deptdoctor deptdoctor);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}

