package lei.query;

public class LimitQuery
{
    private Long page;
    private Long limit;

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "LimitQuery{" +
                "page=" + page +
                ", limit=" + limit +
                '}';
    }
}