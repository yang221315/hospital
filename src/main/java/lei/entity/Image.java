package lei.entity;

import java.io.Serializable;

/**
 * (Image)实体类
 *
 * @author makejava
 * @since 2021-08-19 10:00:00
 */
public class Image implements Serializable {
    private static final long serialVersionUID = 738196682015370688L;
    /**
     * 图片id
     */
    private Long id;
    /**
     * 图片获取路径
     */
    private String imageurl;
    /**
     * 图片标题名称
     */
    private String imagetitle;
    /**
     * 图片类型
     */
    private String imagetype;
    /**
     * 图片状态（1：可用，0不可用）
     */
    private String imagestate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getImagetitle() {
        return imagetitle;
    }

    public void setImagetitle(String imagetitle) {
        this.imagetitle = imagetitle;
    }

    public String getImagetype() {
        return imagetype;
    }

    public void setImagetype(String imagetype) {
        this.imagetype = imagetype;
    }

    public String getImagestate() {
        return imagestate;
    }

    public void setImagestate(String imagestate) {
        this.imagestate = imagestate;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", imageurl='" + imageurl + '\'' +
                ", imagetitle='" + imagetitle + '\'' +
                ", imagetype='" + imagetype + '\'' +
                ", imagestate='" + imagestate + '\'' +
                '}';
    }
}
