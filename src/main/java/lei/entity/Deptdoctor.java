package lei.entity;

import java.io.Serializable;

/**
 * (Deptdoctor)实体类
 *
 * @author makejava
 * @since 2021-08-19 09:59:58
 */
public class Deptdoctor implements Serializable {
    private static final long serialVersionUID = 388746107761671727L;
    /**
     * 医生和科室关联表id
     */
    private Long id;
    /**
     * 关联科室id
     */
    private Long deptid;
    /**
     * 关联医生id
     */
    private Long doctorid;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeptid() {
        return deptid;
    }

    public void setDeptid(Long deptid) {
        this.deptid = deptid;
    }

    public Long getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Long doctorid) {
        this.doctorid = doctorid;
    }

}
