package lei.entity;

import java.io.Serializable;

/**
 * (Department)实体类
 *
 * @author makejava
 * @since 2021-08-19 09:59:55
 */
public class Department implements Serializable {
    private static final long serialVersionUID = -14080995666498208L;
    /**
     * 科室Id
     */
    private Long id;
    /**
     * 科室名字
     */
    private String deptname;
    /**
     * 连接image表
     */
    private String imageurl;
    /**
     * 科室描述
     */
    private String deptdecs;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getDeptdecs() {
        return deptdecs;
    }

    public void setDeptdecs(String deptdecs) {
        this.deptdecs = deptdecs;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", deptname='" + deptname + '\'' +
                ", imageurl='" + imageurl + '\'' +
                ", deptdecs='" + deptdecs + '\'' +
                '}';
    }
}
