package lei.entity;

import java.io.Serializable;

/**
 * (Doctor)实体类
 *
 * @author makejava
 * @since 2021-08-19 09:59:59
 */
public class Doctor implements Serializable {
    private static final long serialVersionUID = 171374860708953881L;
    /**
     * 医生id
     */
    private Long id;
    /**
     * 医生名字
     */
    private String doctorname;
    /**
     * 医生简介
     */
    private String doctortitle;
    /**
     * 医生所属科室id
     */
    private Long deptid;
    /**
     * 医生的职责或功能
     */
    private String doctorduty;
    /**
     * 医生的挂号费用
     */
    private String doctorprice;
    /**
     * 医生照片id
     */
    private Long imageid;

    /**
     * 医生关联照片
     */
    private Image image;

    /**
     * 医生所属科室
     */
    private Department department;

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public String getDoctortitle() {
        return doctortitle;
    }

    public void setDoctortitle(String doctortitle) {
        this.doctortitle = doctortitle;
    }

    public Long getDeptid() {
        return deptid;
    }

    public void setDeptid(Long deptid) {
        this.deptid = deptid;
    }

    public String getDoctorduty() {
        return doctorduty;
    }

    public void setDoctorduty(String doctorduty) {
        this.doctorduty = doctorduty;
    }

    public String getDoctorprice() {
        return doctorprice;
    }

    public void setDoctorprice(String doctorprice) {
        this.doctorprice = doctorprice;
    }

    public Long getImageid() {
        return imageid;
    }

    public void setImageid(Long imageid) {
        this.imageid = imageid;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", doctorname='" + doctorname + '\'' +
                ", doctortitle='" + doctortitle + '\'' +
                ", deptid=" + deptid +
                ", doctorduty='" + doctorduty + '\'' +
                ", doctorprice='" + doctorprice + '\'' +
                ", imageid=" + imageid +
                ", image=" + image +
                ", department=" + department +
                '}';
    }
}
