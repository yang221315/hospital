package lei.entity;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author makejava
 * @since 2021-08-19 10:00:01
 */
public class User implements Serializable {
    private static final long serialVersionUID = -64851478112175904L;
    /**
     * 用户id，自动增加
     */
    private Long id;
    /**
     * 用户微信名称
     */
    private String username;
    /**
     * 用户电话号
     */
    private String phone;
    /**
     * 用户登录密码
     */
    private String password;
    /**
     * 用户注册时的昵称
     */
    private String nickname;
    /**
     * 微信后台返回
     */
    private String openid;
    /**
     * 微信后台返回
     */
    private String sessionkey;
    /**
     * 由openid和sessionkey加密获得
     */
    private String token;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getSessionkey() {
        return sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
