package lei.entity;

import java.io.Serializable;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2021-08-19 10:00:01
 */
public class Order implements Serializable {
    private static final long serialVersionUID = -63770562023674816L;
    /**
     * 预约表id
     */
    private Long id;
    /**
     * 关联user表的openid
     */
    private String openid;
    /**
     * 预约者的名字
     */
    private String username;
    /**
     * 预约事件的状态
     */
    private String orderstate;
    /**
     * 预约的时间
     */
    private String ordertime;
    /**
     * 预约者的电话
     */
    private String userphone;
    /**
     * 预约的医生id
     */
    private Long doctorid;
    /**
     * 预约的科室id
     */
    private Long deptid;
    /**
     * 预约者的留言信息
     */
    private String information;

    private String name;

    private String url;


    private Doctor doctor;

    private Department department;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrderstate() {
        return orderstate;
    }

    public void setOrderstate(String orderstate) {
        this.orderstate = orderstate;
    }

    public String getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime;
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }

    public Long getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Long doctorid) {
        this.doctorid = doctorid;
    }

    public Long getDeptid() {
        return deptid;
    }

    public void setDeptid(Long deptid) {
        this.deptid = deptid;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", openid='" + openid + '\'' +
                ", username='" + username + '\'' +
                ", orderstate='" + orderstate + '\'' +
                ", ordertime='" + ordertime + '\'' +
                ", userphone='" + userphone + '\'' +
                ", doctorid=" + doctorid +
                ", deptid=" + deptid +
                ", information='" + information + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", doctor=" + doctor +
                ", department=" + department +
                '}';
    }
}
