// pages/register/register.js
var httpUrl = "http://39.108.59.6:8080/BookHospital"
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  submit:function(e){
    var that = this;
    console.log("注册",e.detail.value)
    //获取授权之后的昵称和头像
    wx.getStorage({
      key: 'userInfo',
      success (res) {
        //发送请求，完成注册  
          wx.request({
            url: httpUrl+'/user/regUser', //仅为示例，并非真实的接口地址
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
              username:e.detail.value.username,
              phone:e.detail.value.phone,
              password:e.detail.value.password,
              nickname:res.data.nickName,
              imageurl:res.data.avatarUrl
            },
            success (res) {
              console.log("注册账号返回值",res.data.code);
              if(res.data.code!='0'){
                  //失败了，显示失败信息，重新注册
                  wx.showToast({
                    title:res.data.msg,
                    icon: 'error',
                    duration: 2000
                  })
              }else{
                //调整页面
                wx.navigateTo({
                  url: "/pages/login/login"
                })

              }
            }
          })
      }
    })
    

  },
  loginView:function(){
    wx.navigateTo({
      url: "/pages/login/login"
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})