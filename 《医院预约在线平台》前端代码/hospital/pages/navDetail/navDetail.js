// pages/navDetail/navDetail.js
var httpUrl = "http://39.108.59.6:8080/BookHospital"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    typeTitle:"",
    page:1,
    limit:4,
    isDate:true,
    dataList:[],
    urlImage:"http://39.108.59.6:8080/BookHospital/img/",
  },
  //去到订单页面
  orderTap:function(even){
    console.log("id是",even.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/order/order?tecid='+even.currentTarget.dataset.id,    
    })
  },

  //去到详情页面
  proDetail:function(even){
    console.log("点击产品",even.currentTarget.dataset.proid);
    wx.navigateTo({
      url: '/pages/tecDetail/tecDetail?proid='+even.currentTarget.dataset.proid
    })
  },

  //提交表单数据进行模糊查询
  submit:function(even){
    var that = this;
    // console.log(even);
    var keyword = even.detail.value.keyword;
    console.log("查询产品",keyword);
    //发送请求  根据产品名称查询数据
    wx.request({
      url: httpUrl+'/doctor/queryDocByKeyword', 
      data: {
        keyword: keyword,
        deptname:that.data.typeTitle,
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success (res) {
        console.log(res.data.data);
        that.setData({
          dataList:res.data.data
        })
      }
    })
    

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("nav详情页面："+options.type);
    var that = this;
    that.setData({
      typeTitle:options.type
    })
    
    //发送请求，获取分类信息
    this.getData(options.type,that.data.page,that.data.limit);
  },
  lower:function(){
    var that = this;
    that.data.page++;
    //发起请求
    this.getData(that.data.typeTitle,that.data.page,that.data.limit);
  },

  getData:function(type,page,limit){
    var that = this;
    if(that.data.isDate==true){
      wx.request({
        url: httpUrl+'/doctor/queryDocByDept', 
        data: {
          deptname:type,
          page:page,
          limit:limit
        },
        header: {
          'content-type': 'application/json' // 默认值
        },
        success (res) {
         console.log(res.data.data)
          var proList = [...that.data.dataList,...res.data.data]
  
          if(res.data.code=="0"){
            that.setData({
              dataList:proList,
              isDate:res.data.count>proList.length
            })
          }
        }
      })
    }
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})