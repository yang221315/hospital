// pages/start/start.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取本地缓存的用户信息，如果获取到了，那么直接跳转到首页
    wx.getStorage({
      key: 'userInfo',
      success (res) {
        //说明已经授权，没有过期，直接到首页
        wx.switchTab({
          url: '/pages/index/index'
        })
      }
    })

  },

  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res.userInfo);
        //1.放入本地缓存
        wx.setStorage({
          key:"userInfo",
          data:res.userInfo,
         
        })
         wx.switchTab({
          url: '/pages/index/index'
        })


        //发送登录请求
        // wx.login({
        //   success (result) {
        //     if (result.code) {
        //       //发起网络请求
        //       wx.request({
        //         url: 'http://127.0.0.1/user/userLogin',
        //         data: {
        //           code: result.code,
        //           nickname:res.userInfo.nickName,
        //           imageUrl:res.userInfo.avatarUrl
        //         },
        //         success:function(e){
        //           console.log("登录响应",e.data.data);
        //           //放入本地缓存
        //           wx.setStorage({
        //             key:"loginInfo",
        //             data:e.data.data
        //           })
        //         }
        //       })
        //     } else {
        //       console.log('登录失败！' + result.errMsg)
        //     }
        //   }
        // })




        // //2.跳转页面  -- 首页   tabbar页面
        // wx.switchTab({
        //   url: '/pages/index/index'
        // })
        


      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})