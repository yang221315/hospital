// pages/order/order.js
var httpUrl = "http://39.108.59.6:8080/BookHospital"
var util = require("../../utils/util.js")

var docid = 0;
var deptid = 0;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imageUrl:"http://39.108.59.6:8080/BookHospital/img/",
    curentData:util.formatD(new Date()),
    curentTime:"9:00",
    tecid:0,
    url:"",
    name:"",
    weizhi:"",
    doctor:"",
    id:""
  },
  bindDateChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    //console.log(date);
    this.setData({
      curentData: e.detail.value
    })
  },
  bindTimeChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      curentTime: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    //接收上一个页面传递的数据
    console.log("科室id = ",options.id)
    console.log("医师id = ",options.tecid);
      //根据商家id获取商家信息    data.data.busid
     if(typeof(options.tecid)=="undefined")
     {
       this.fun2(options.id);
       deptid = options.id;
     }
     else
     {
       this.fun1(options.tecid);
       docid = options.tecid;
     }
  },

  //只有医生Id,没有科室id
  fun1(id)
  {
    var that = this;
    wx.request({
      url: httpUrl+'/doctor/queryInfoByDocId', 
      data: {
        id:id
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success (res) {
        console.log("获取产品对应的信息",res.data.data);
        that.setData({
          doctor:res.data.data[0].doctorname,
          url:res.data.data[0].image.imageurl,
          name:res.data.data[0].department.deptname,
          weizhi:res.data.data[0].doctortitle,
          id:res.data.data[0].id,
        })
      }
    })
  },

  //只有科室id没有医生Id
  fun2(id)
  {
    var that = this;
    wx.request({
      url: httpUrl+'/department/queryDepInfoById', 
      data: {
        id:id
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success (res) {
        console.log("获取产品对应的信息",res.data.data);
        that.setData({
          doctor:"暂无信息",
          url:res.data.data.imageurl,
          name:res.data.data.deptname,
          weizhi:"暂无信息",
          id:res.data.data.id,
        })
      }
    })
  },

  submit:function(even){
    var that = this;
    //1.获取表单的数据
    console.log("提交表单",even.detail.value);
    //2.准备其他数据 token    产品名称  预约时间  商家id   产品id  技师id
    wx.checkSession({
      success () {
        //获取本地的token
        wx.getStorage({
          key: 'token',
          success (res) {
            var tecid = that.data.tecid;
            console.log("获取token = ",res.data+",获取tecid："+that.data.tecid)
            if(typeof(tecid)=="undefined"){
              tecid = 0;
            }
            //获取成功，发送请求
            wx.request({
              url: httpUrl+'/order/addOrder', 
              method:"POST",
              data: {
                token:res.data,
                ordertime:that.data.curentData+" "+that.data.curentTime,
                username:even.detail.value.username,
                userphone:even.detail.value.usertell,
                information:even.detail.value.information,
                deptid:deptid,
                doctorid:docid,
              },
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success (res) {
                console.log(res.data);
                if(res.data.code==0){
                    //直接跳转页面
                    wx.switchTab({
                      url: '/pages/my/my'
                    })
                }else{
                  //显示错误信息
                  wx.showToast({
                    title: res.data.msg,
                    icon: 'error',
                    duration: 2000
                  })
                }
              }
            })

          },
          fail:function(){
            //获取token失败
            wx.navigateTo({
              url: "/pages/login/login"
              
            })

          }
          

        })

      },
      fail () {
        // session_key 已经失效，需要重新执行登录流程  ，跳转到登录页面
        wx.navigateTo({
          url: "/pages/login/login"
        })
        
      }
    })  

   

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})