// pages/my/my.js
var httpUrl = "http://39.108.59.6:8080/BookHospital"
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLogin: app.globalData.isLogin,
    navTab: ['过期','待处理','已处理','预定中'],        
    currentTab: 0,
    sendList:[],
    imageUrl:"http://39.108.59.6:8080/BookHospital/img/",
    userInfo:{},
    Url:getApp().globalData.url
  },

  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log(app.globalData.isLogin);
    wx.checkSession({
      success () {
        //session_key 未过期，并且在本生命周期一直有效
        console.log("已经登录");
      },
      fail () {
        // session_key 已经失效，需要重新执行登录流程
        console.log("重新登录");
      }
    })
    //本地缓存获取数据
    wx.getStorage({
      key: 'userInfo',
      success (res) {
        console.log("从本地缓存获取微信用户信息",res.data);
        that.setData({
          userInfo:res.data
        })
      }
    })

  //发送请求  默认全部订单
  wx.getStorage({
    key: 'token',
    success (res) {
      console.log("========",res.data);

        //2.发送请求，获取对应信息   根据订单的状态获取信息
        //参数：订单的状态    用户的openid（userid订单表）
        //发送请求
        wx.request({
          url: httpUrl+'/order/queryOrderByType', //仅为示例，并非真实的接口地址
          data: {
            orderstate: "0",
            token:res.data
          },
          header: {
            'content-type': 'application/json' // 默认值
          },
          success:function(res){
            console.log("获取订单信息:",res.data.data);
            that.setData({
              sendList:res.data.data
            })
          }
          
        })


    }
  })
  },

  curTab1:function(even){
    var that = this;
    var orderstate = "";

    var index = even.currentTarget.dataset.idx;
    console.log("点击的角标",even.currentTarget.dataset.idx);
    this.setData({
      currentTab:even.currentTarget.dataset.idx
    })

    if(index==0){
      orderstate="0";
    }else if(index==1){
      orderstate="1";
    }else if(index==2){
      orderstate="2";
    }else if(index==3){
      orderstate="3";
    }
  
    wx.getStorage({
      key: 'token',
      success (res) {
        console.log("========",res.data);

          //2.发送请求，获取对应信息   根据订单的状态获取信息
          //参数：订单的状态    用户的openid（userid订单表）
          //发送请求
          wx.request({
            url: httpUrl+'/order/queryOrderByType', 
            data: {
              orderstate: orderstate,
              token:res.data
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            success:function(res){
              console.log("获取订单信息:",res.data.data);
              that.setData({
                sendList:res.data.data
              })
            }
            
          })


      }
    })

    

  },

  loginView:function(){
    wx.navigateTo({
      url: "/pages/login/login"
    })
  },
  regView:function(){
    wx.navigateTo({
      url: "/pages/register/register"
    })
  },
  curTab:function(even){
    var that = this;
    console.log(even.currentTarget.dataset.idx);
    var idx = even.currentTarget.dataset.idx;
    this.setData({
      currentTab:even.currentTarget.dataset.idx
    })

    var ordertype = "";
    if(idx==0){
      ordertype = "";
    }else if(idx==1){
      ordertype = "0";
    }else if(idx==2){
      ordertype = "2";
    }else if(idx == 3){
      ordertype = "4";
    }
    console.log("订单状态：",ordertype);
    //发送请求
    wx.request({
      url: httpUrl+'/order/queryOrder', 
      data: {
        ordertype: ordertype
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success (res) {
        console.log(res.data);
        that.setData({
          sendList:res.data.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      isLogin:app.globalData.isLogin
    })
  },

  //退出登录状态
  tuichu()
  {
    app.globalData.isLogin = false;
    this.setData({
      isLogin:app.globalData.isLogin
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})