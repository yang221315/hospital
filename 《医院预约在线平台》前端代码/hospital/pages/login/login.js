// pages/login/login.js
var httpUrl = "http://39.108.59.6:8080/BookHospital"
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },
  regView:function(){
    wx.navigateTo({
      url: "/pages/register/register"
    })
  },
  //用户登录
  submit:function(e){
    console.log("用户点击登录:",JSON.stringify(e.detail.value));
    wx.login({
      success (res) {
        if (res.code) {
          //发起网络请求
          wx.request({
            url: httpUrl+'/user/userLogin',
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
              code: res.code,
              phone:e.detail.value.phone,
              password:e.detail.value.password

            },
            success:function(res){
              console.log("用户登录返回",res)
              if(res.data.code=='0'){//成功
                  app.globalData.isLogin = true;
                  console.log(app.globalData.isLogin)
                  //获取返回值
                  var token = res.data.data;//返回的token
                  //放入本地缓存
                  wx.setStorage({
                    key:"token",
                    data:res.data.data
                  })
                  //跳转页面  我的页面
                  wx.switchTab({
                    url: '/pages/my/my'
                  })

              }else{
                wx.showToast({
                  title: res.data.msg,
                  icon: 'error',
                  duration: 2000
                })
              }
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})